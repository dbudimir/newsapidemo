package db.newsapidemo.ui.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import db.newsapidemo.repository.NewsRepository

/**
 * Created by Dario Budimir on 17/04/18.
 */

public class NewsViewModel(private val newsInterface: NewsRepository) : ViewModel() {
    private val topic = MutableLiveData<String>()
    private val repoResult = Transformations.map(topic, {
        newsInterface.news(it, 20)
    })

    val news = Transformations.switchMap(repoResult, { it.pagedList })!!
    val networkState = Transformations.switchMap(repoResult, { it.networkState })!!
    val refreshState = Transformations.switchMap(repoResult, { it.refreshState })!!

    fun refresh() {
        repoResult.value?.refresh?.invoke()
    }

    fun showNews(topic: String): Boolean {
        if (this.topic.value == topic) {
            return false
        }
        this.topic.value = topic
        return true
    }

    fun retry() {
        val listing = repoResult?.value
        listing?.retry?.invoke()
    }

    fun currentTopic(): String? = topic.value
}