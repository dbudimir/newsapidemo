package db.newsapidemo.ui.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import db.newsapidemo.ui.fragment.BusinessFragment
import db.newsapidemo.ui.fragment.SearchFragment
import db.newsapidemo.ui.fragment.TechnologyFragment

/**
 * Created by Dario Budimir on 17/04/18.
 */

class TabPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                BusinessFragment()
            }
            1 -> TechnologyFragment()
            else -> {
                return SearchFragment()
            }
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Business"
            1 -> "Technology"
            else -> {
                return "Search"
            }
        }
    }
}
