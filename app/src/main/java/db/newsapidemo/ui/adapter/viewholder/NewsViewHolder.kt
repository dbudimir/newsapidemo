package db.newsapidemo.ui.adapter.viewholder

import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.RequestManager
import db.newsapidemo.R
import db.newsapidemo.models.ArticlesItem

/**
 * Created by Dario Budimir on 17/04/18.
 */

class NewsViewHolder(view: View, private val glide: RequestManager) : RecyclerView.ViewHolder(view) {
    private val title: TextView = view.findViewById(R.id.title)
    private val subtitle: TextView = view.findViewById(R.id.subtitle)
    private val thumbnail : ImageView = view.findViewById(R.id.thumbnail)
    private var newsItem : ArticlesItem? = null
    init {
        view.setOnClickListener {
            newsItem?.url?.let { url ->
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                view.context.startActivity(intent)
            }
        }
    }

    fun bind(item: ArticlesItem?) {
        this.newsItem = item
        title.text = item?.title ?: "none"
        subtitle.text = item?.description
        if (item?.urlToImage?.startsWith("http") == true) {
            thumbnail.visibility = View.VISIBLE
            glide.load(item?.urlToImage)
                    .into(thumbnail)
        } else {
            thumbnail.setImageResource(android.R.drawable.ic_menu_gallery)
        }

    }

    companion object {
        fun create(parent: ViewGroup, glide: RequestManager): NewsViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.news_item, parent, false)
            return NewsViewHolder(view, glide)
        }
    }

    fun updateScore(item: ArticlesItem?) {
        newsItem = item
    }
}