package db.newsapidemo.ui.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.arch.paging.PagedList
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import com.bumptech.glide.Glide
import db.newsapidemo.R
import db.newsapidemo.api.NewsAPI
import db.newsapidemo.models.ArticlesItem
import db.newsapidemo.repository.NewsItemsRepository
import db.newsapidemo.ui.viewmodel.NewsViewModel
import db.newsapidemo.ui.adapter.NewsAdapter
import kotlinx.android.synthetic.main.fragment_search.*
import java.util.concurrent.Executors

/**
 * Created by Dario Budimir on 17/04/18.
 */

class SearchFragment : Fragment() {

    private lateinit var model: NewsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        model = getViewModel()
        initAdapter()
        initSearch()
    }

    private fun getViewModel(): NewsViewModel {
        return ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                val repo = NewsItemsRepository(NewsAPI.create(), Executors.newFixedThreadPool(5))
                @Suppress("UNCHECKED_CAST")
                return NewsViewModel(repo) as T
            }
        })[NewsViewModel::class.java]
    }

    private fun initAdapter() {
        val glide = Glide.with(this)
        val adapter = NewsAdapter(glide) {
            model.retry()
        }
        searchList.adapter = adapter
        model.news.observe(this, Observer<PagedList<ArticlesItem>> {
            adapter.submitList(it)
            Log.d("TAGG","bbbbbbbbbb")
        })
        model.networkState.observe(this, Observer {
            adapter.setNetworkState(it)
        })
    }

    private fun initSearch() {
        input.setOnEditorActionListener({ _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_GO) {
                updatedNewsFromInput()
                true
            } else {
                false
            }
        })
        input.setOnKeyListener({ _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                updatedNewsFromInput()
                true
            } else {
                false
            }
        })
    }

    private fun updatedNewsFromInput() {
        input.text.trim().toString().let {
            if (it.isNotEmpty()) {
                if (model.showNews(it)) {
                    searchList.scrollToPosition(0)
                    (searchList.adapter as? NewsAdapter)?.submitList(null)
                }
            }
        }
    }
}