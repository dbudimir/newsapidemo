package db.newsapidemo.repository

import db.newsapidemo.models.ArticlesItem
import db.newsapidemo.models.NewsResponse

/**
 * Created by Dario Budimir on 17/04/18.
 */

interface NewsRepository {
    fun news(topic: String, pageSize: Int): NewsListing<ArticlesItem>
}