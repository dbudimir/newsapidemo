package db.newsapidemo.repository.datasource

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PageKeyedDataSource
import db.newsapidemo.api.NewsAPI
import db.newsapidemo.models.ArticlesItem
import db.newsapidemo.models.NewsResponse
import db.newsapidemo.repository.NetworkState
import retrofit2.Call
import retrofit2.Response
import java.io.IOException
import java.util.concurrent.Executor

/**
 * Created by Dario Budimir on 17/04/18.
 */

class NewsDataSource(
        private val newsApi: NewsAPI,
        private val topic: String,
        private val retryExecutor: Executor) : PageKeyedDataSource<String, ArticlesItem>() {

    private var retry: (() -> Any)? = null

    val networkState = MutableLiveData<NetworkState>()

    val initialLoad = MutableLiveData<NetworkState>()

    fun retryAllFailed() {
        val prevRetry = retry
        retry = null
        prevRetry?.let {
            retryExecutor.execute {
                it.invoke()
            }
        }
    }

    override fun loadBefore(
            params: LoadParams<String>,
            callback: LoadCallback<String, ArticlesItem>) {
            // ignored, since we only ever append to our initial load
    }

    override fun loadAfter(params: LoadParams<String>, callback: LoadCallback<String, ArticlesItem>) {
        networkState.postValue(NetworkState.LOADING)
        newsApi.getNews(topic = topic,
                pageSize = params.requestedLoadSize).enqueue(
                object : retrofit2.Callback<NewsResponse> {
                    override fun onFailure(call: Call<NewsResponse>, t: Throwable) {
                        retry = {
                            loadAfter(params, callback)
                        }
                        networkState.postValue(NetworkState.error(t.message
                                ?: "unknown error"))
                    }

                    override fun onResponse(
                            call: Call<NewsResponse>,
                            response: Response<NewsResponse>) {
                        if (response.isSuccessful) {
                            val items = response.body()!!.articles
                            retry = null
                            callback.onResult(items, params.key)
                            networkState.postValue(NetworkState.LOADED)
                        } else {
                            retry = {
                                loadAfter(params, callback)
                            }
                            networkState.postValue(
                                    NetworkState.error("error code: ${response.code()}"))
                        }
                    }
                }
        )
    }

    override fun loadInitial(
            params: LoadInitialParams<String>,
            callback: LoadInitialCallback<String, ArticlesItem>) {
        val request = newsApi.getNews(
                topic = topic,
                pageSize = params.requestedLoadSize
        )
        networkState.postValue(NetworkState.LOADING)
        initialLoad.postValue(NetworkState.LOADING)

        // triggered by a refresh, we better execute sync
        try {
            val response = request.execute()
            val items = response.body()
            retry = null
            networkState.postValue(NetworkState.LOADED)
            initialLoad.postValue(NetworkState.LOADED)
            callback.onResult(items!!.articles, "1", params.requestedLoadSize.toString())
        } catch (ioException: IOException) {
            retry = {
                loadInitial(params, callback)
            }
            val error = NetworkState.error(ioException.message
                    ?: "unknown error")
            networkState.postValue(error)
            initialLoad.postValue(error)
        }
    }
}