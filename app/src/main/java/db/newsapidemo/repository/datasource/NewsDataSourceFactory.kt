package db.newsapidemo.repository.datasource

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import db.newsapidemo.api.NewsAPI
import db.newsapidemo.models.ArticlesItem
import db.newsapidemo.models.NewsResponse
import java.util.concurrent.Executor

/**
 * Created by Dario Budimir on 17/04/18.
 */

class NewsDataSourceFactory(
        private val newsApi: NewsAPI,
        private val topic: String,
        private val retryExecutor: Executor) : DataSource.Factory<String, ArticlesItem>() {
    val sourceLiveData = MutableLiveData<NewsDataSource>()
    override fun create(): DataSource<String, ArticlesItem> {
        val source = NewsDataSource(newsApi, topic, retryExecutor)
        sourceLiveData.postValue(source)
        return source
    }
}