package db.newsapidemo.repository

import android.arch.lifecycle.Transformations
import android.arch.paging.LivePagedListBuilder
import android.support.annotation.MainThread
import db.newsapidemo.api.NewsAPI
import db.newsapidemo.models.ArticlesItem
import db.newsapidemo.models.NewsResponse
import db.newsapidemo.repository.datasource.NewsDataSourceFactory
import java.util.concurrent.Executor

/**
 * Created by Dario Budimir on 17/04/18.
 */

class NewsItemsRepository (private val newsApi: NewsAPI, private val networkExecutor: Executor) : NewsRepository {
    @MainThread
    override fun news(topic: String, pageSize: Int): NewsListing<ArticlesItem> {
        val sourceFactory = NewsDataSourceFactory(newsApi, topic, networkExecutor)

        val livePagedList = LivePagedListBuilder(sourceFactory, pageSize)
                // provide custom executor for network requests, otherwise it will default to
                // Arch Components' IO pool which is also used for disk access
                .setFetchExecutor(networkExecutor)
                .build()

        val refreshState = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.initialLoad
        }
        return NewsListing(
                pagedList = livePagedList,
                networkState = Transformations.switchMap(sourceFactory.sourceLiveData, {
                    it.networkState
                }),
                retry = {
                    sourceFactory.sourceLiveData.value?.retryAllFailed()
                },
                refresh = {
                    sourceFactory.sourceLiveData.value?.invalidate()
                },
                refreshState = refreshState
        )
    }
}
