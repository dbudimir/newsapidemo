package db.newsapidemo.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Dario Budimir on 17/04/18.
 */

data class Source(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Any? = null
)