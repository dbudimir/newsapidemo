package db.newsapidemo.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Dario Budimir on 17/04/18.
 */

data class NewsResponse(

	@field:SerializedName("totalResults")
	val totalResults: Int? = null,

	@field:SerializedName("articles")
	val articles: List<ArticlesItem> = emptyList(),

	@field:SerializedName("status")
	val status: String? = null
)