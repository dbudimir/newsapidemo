package db.newsapidemo.api

import android.util.Log
import db.newsapidemo.BuildConfig
import db.newsapidemo.models.NewsResponse
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by Dario Budimir on 17/04/18.
 */

interface NewsAPI {

    @GET("everything")
    fun getNews(
            @Query("q") topic: String,
            @Query("pageSize") pageSize: Int): Call<NewsResponse>


    companion object {
        private const val API_KEY_PARAMETER = "apiKey"

        fun create(): NewsAPI = create(HttpUrl.parse(BuildConfig.BASE_URL)!!)
        fun create(httpUrl: HttpUrl): NewsAPI {
            val client = OkHttpClient.Builder()
                    .addInterceptor(HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY))
                    .addInterceptor(provideInterceptor())
                    .build()

            return Retrofit.Builder()
                    .baseUrl(httpUrl)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(NewsAPI::class.java)
        }

        fun provideInterceptor(): Interceptor {
            return Interceptor { chain ->
                //copy and alter url
                val url = chain
                        .request()
                        .url()
                        .newBuilder()
                        .addQueryParameter(API_KEY_PARAMETER, BuildConfig.API_KEY)
                        .build()

                //copy and alter request
                val request = chain.request()
                        .newBuilder()
                        .url(url)
                        .build()

                chain.proceed(request)
            }
        }
    }
}